﻿
using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class CategoriesLogic : BaseLogic {
        public List<CategoryModel> GetAllCategories() {
            return DB.Categories.Select(c => new CategoryModel {
                id = c.CategoryID,
                name = c.CategoryName
            }).ToList();
        }


        public List<ClothingModel> GetClothesByCategories(int id) {
            return DB.Clothes.Include("Categories").Include("Types").Include("Companies")
                .Where(c => c.CategoryID == id)
                .Select(c => new ClothingModel {
                id = c.ClothID,
                category = new CategoryModel { id = c.Category.CategoryID, name = c.Category.CategoryName },
                company = new CompanyModel { id = c.Company.CompanyID, name = c.Company.CompanyName },
                type = new TypeModel { id = c.Type.TypeID, name = c.Type.TypeName },
                price = c.Price,
                discount = c.Discount,
                image = c.Image
            }).ToList();
        }

        
    }
}