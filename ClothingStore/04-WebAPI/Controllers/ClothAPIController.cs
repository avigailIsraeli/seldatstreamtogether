﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat {
    [EnableCors("*", "*", "*")]
    public class ClothAPIController : ApiController {
        private ClothingLogic logic = new ClothingLogic();

        [HttpGet]
        [Route("api/clothing")]
        public HttpResponseMessage GetClothes() {
            try {
                List<ClothingModel> clothes = logic.GetAllClothes();
                return Request.CreateResponse(HttpStatusCode.OK, clothes);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpGet]
        [Route("api/clothing/{id}")]
        public HttpResponseMessage GetCloth(int id) {
            try {
                ClothingModel cloth = logic.GetOneCloth(id);
                return Request.CreateResponse(HttpStatusCode.OK, cloth);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpPut]
        [Route("api/clothing/{id}")]
        public HttpResponseMessage UpdateCloth([FromUri]int id) {
            try {
                float discount = 0;
                int categoryID = int.Parse(HttpContext.Current.Request.Form["categoryIDEdit"]);
                string categoryName = HttpContext.Current.Request.Form["categoryNameEdit"];
                int companyID = int.Parse(HttpContext.Current.Request.Form["companyIDEdit"]);
                string companyName = HttpContext.Current.Request.Form["companyNameEdit"];
                int typeID = int.Parse(HttpContext.Current.Request.Form["typeIDEdit"]);
                string typeName = HttpContext.Current.Request.Form["typeNameEdit"];
                decimal price = Convert.ToDecimal(HttpContext.Current.Request.Form["priceEdit"]);
                if (HttpContext.Current.Request.Form["discountEdit"] != "")
                    discount = Convert.ToSingle(HttpContext.Current.Request.Form["discountEdit"]);
                string newFileName = "";
                if (HttpContext.Current.Request.Files.Count > 0) {
                    string originalName = HttpContext.Current.Request.Files[0].FileName;
                    newFileName = Guid.NewGuid().ToString() + Path.GetExtension(originalName);
                    string fullPathAndFileName = HttpContext.Current.Server.MapPath("~/Images/" + newFileName);
                    HttpContext.Current.Request.Files[0].SaveAs(fullPathAndFileName);
                }
                else {
                    newFileName = HttpContext.Current.Request.Form["imageEdit"];
                }
                ClothingModel cloth = logic.UpdateFullCloth(new ClothingModel { id = id, category = new CategoryModel { id = categoryID, name = categoryName }, company = new CompanyModel { id = companyID, name = companyName }, type = new TypeModel { id = typeID, name = typeName }, price = price, discount = discount, image = newFileName });
                return Request.CreateResponse(HttpStatusCode.OK, cloth);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                                    ex.GetUserFriendlyMessage());
            }
        }


        [HttpDelete]
        [Route("api/clothing/{id}")]
        public HttpResponseMessage DeleteCloth([FromUri]int id) {
            try {
                string path= HttpContext.Current.Server.MapPath("~/Images/" + logic.GetOneCloth(id).image);
                File.Delete(path);
                logic.DeleteCloth(id);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    ex.GetUserFriendlyMessage());

            }
        }

        [HttpPost]
        [Route("api/clothing")]
        public HttpResponseMessage AddCloth(ClothingModel cloth) {

            try {
                if (!ModelState.IsValid) {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetAllErrors());
                }
                cloth = logic.AddCloth(cloth);
                return Request.CreateResponse(HttpStatusCode.Created, cloth);
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpPost]
        [Route("api/upload")]
        public HttpResponseMessage UploadImage() {
            try {
                int clothingID = int.Parse(HttpContext.Current.Request.Form["clothingID"]);
                    string originalName = HttpContext.Current.Request.Files[0].FileName;
                    string newFileName = Guid.NewGuid().ToString() + Path.GetExtension(originalName);
                    string fullPathAndFileName = HttpContext.Current.Server.MapPath("~/Images/" + newFileName);
                    HttpContext.Current.Request.Files[0].SaveAs(fullPathAndFileName);
               ClothingModel clothing=logic.saveImage(clothingID, newFileName);
                    return Request.CreateResponse(HttpStatusCode.Created,clothing);           
            }
            catch (Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing)
                logic.Dispose();
            base.Dispose(disposing);
        }
    }
}


